# YouTube Playlist Builder

Google Apps script for automating youtube playlist creation/updating based on gmail notifications. The script search your gmail for the notificaitons that Youtube sends out about new videos. Note, this does not give me access to your emails in any way, it will be your copy of the script that is doing the searching.

Instructions for setup are in the script. 

Create your own and copy the code from PlaylistBuilder.gs into it or open the [source script](https://script.google.com/d/1BXqO_0u_eX_qqPMt9g6RHnBTieczdnnH2bFdjEvOvO5cPStalQoDtY85/edit?usp=sharing) and duplicate to your own drive.