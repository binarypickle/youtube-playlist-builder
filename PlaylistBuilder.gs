// www.benjaminworrel.com

// based on original script by
// http://www.reddit.com/user/DiRaven
// https://www.reddit.com/r/youtube/comments/3ukn4w/automatically_adding_youtube_videos_to_watch/
// January 2016


//changelog

//2016-09-28
//Updated removeOldVideos function to use the date the video was published rather than the date it was added to the playlist
//Updated the email search to use the 'daysToSave' variable. If value is entered, search will look for emails back to that day (assuming I'm not off by one). If not set, no date parameter will be used and all matching emails will be used.
//Fixed weirdness where I was checking the state of 'targetPlaylistId' before declaring logging variables. (shrug)
//Fixed multi-query search

//2016-10-10
//Removed Watch Later playlist instruction

//2017-07-21
//Adding some additional instruction text


// Let's do some setup 


// Before we get into the script settings, make sure you have email notifications turned on for the youtube channels you want to add to your playlists
// We use those emails to actually find the videos
// This also means this script won't do much until you've gotten any emails

// Ok then, first, you'll need to copy the script to your Google Drive. Can't go editing my version now can you?
// Open up the File menu, and select Make A Copy

// Next, you need to make sure Google Advanced Services is enabled
// Open up the Resources menu, and select Advanced Google Services
// Scroll down to YouTube Data API and toggle it to "on"
// Next, click the Google Developers Console link at the bottom of the Advanced Services window to open up the API Manager
// Search for youtube, select YouTube Data API v3 and select enable
// In theory, that should do it. Now back to the script setup.

// Set the playlist you want to update. Change targetPlaylistId (below this) to the ID of the playlist you wish to use, as this is mine and won't work for you
// You can get this from the url. Example: https:// www.youtube.com/playlist?list=PLnyPWSfLMCN2KrTPXp7Xr1L8slXBLNanS
// You'll need to make a playlist to use. Youtube has removed access to the default Watch Later playlist.
var targetPlaylistId='PLnyPWSfLMCN2KrTPXp7Xr1L8slXBLNanS';

// Set number of days (loosely) you want to hold onto videos
// If you don't want to automatically clear old videos, comment it out with two //
var daysToSave = 3;

// Setting the correct timezone won't impact things too much, but will help determine when videos are "new" or "old"
var timezone = "CST";

// Should we ignore upcoming live stream announcements? I'd advise leaving this set to true.
var ignoreUpcomingLive = true;

// Set the minimum duration, in seconds, for videos to be included. 
// If you don't care, comment it out 
var minDuration = 30;

// Set the maximum duration, in seconds, for videos to be included
// If you don't care, comment it out 
var maxDuration = 5400;


// Setup the gmail searches to find desired YouTube notifications
// The two queries I've included will catch pretty much all notifications, 
// but if you want to get fancy and build a playlist for a specific set of channels you can alter the search to be specific
// For example, if you want all of the videos from the rather excellent CrashCourse (www.youtube.com/user/crashcourse), you could use the following
// emailQueries.push('from:"noreply@youtube.com" subject:"CrashCourse"');
// These queries are built just like a search in gmail. Be careful of your single and double quotes
// Add additional lines for additional queries
// Note: if you want to build multiple playlists, you'll need multiple copies of this script (each named something else)

var emailQueries = new Array();
emailQueries.push('from:"noreply@youtube.com" subject:"uploaded a video"');
emailQueries.push('from:"noreply@youtube.com" subject:"new videos from"');

// If you want to keep a log of script actions, change logsheetId to the google spreadsheet to store the log in. One sheet can be shared across multiple playlist scripts if desired.
// You can get the ID from the URL, 
// If not logging, comment it out with //
// Note: this will eventually fill up and you'll either need to clear the spreadsheet or start a new one if you're really into logging
var logsheetId = '1yQOk9IwMNaXhhxDaxOX4nQEyCb-AzHAUCqzX7TzQVsE';

// Run this once to grant script access and add a trigger to run the script automatically every hour
// You can run a function from the Run menu
// It will ask for permissions. Youtube for access to the playlists, gmail for access to email to find & process the notification emails, and spreadsheets for logging
function AddTrigger()
{
  ScriptApp.newTrigger('refreshVideos')
  .timeBased()
  .everyHours(1)
  .create();
}


/**
//
// Setup is done. You can manually run the script or wait for the time trigger.
//
**/

  var activeLogsheet;
  var playlistName;


// Remove old videos and add new videos
function refreshVideos()
{
  if(typeof logsheetId !== 'undefined'){
    var logsheetResponse = SpreadsheetApp.openById(logsheetId);
    activeLogsheet = logsheetResponse.getSheets()[0];
    var playlistResponse = YouTube.Playlists.list('snippet',{id:targetPlaylistId,maxResults:1});
    playlistName = playlistResponse.items[0].snippet.title;
    activeLogsheet.appendRow([Utilities.formatDate(new Date(), timezone, "yyyy-MM-dd HH:mm:ss '" + timezone +"'"),"Refresh Playlist",playlistName]);    
  }
  
  // clear old videos
  if(typeof daysToSave !== 'undefined' && daysToSave >= 0){
    removeOldVideos(daysToSave);
  }
  
  // run the gmail searches and collect results
  var videoEmails = new Array();
  
  //set how far back in emails to look
  var searchDate = new Date();
  var optionsString = '';
  if(typeof daysToSave !== 'undefined' && daysToSave >= 0){
    searchDate.setDate(searchDate.getDate()-(daysToSave-1));
    optionsString = ' after:'+ Utilities.formatDate(searchDate, timezone, "yyyy/MM/dd");
  }
  
   for(var queryIndex = 0; queryIndex < emailQueries.length; queryIndex++){
    if(queryIndex == 0){
      videoEmails = GmailApp.search(emailQueries[queryIndex] + optionsString );
      
    }
    else{
      var additionalThreads = GmailApp.search(emailQueries[queryIndex]+ optionsString);
      for(var i in additionalThreads){
        
        videoEmails.push(additionalThreads[i]);
      }
    }
  }
  
  // if emails were found, process them to find watch links
  if (videoEmails.length > 0)
  {
    processThreads(videoEmails);
  }
} //end refresh videos function


// scrape emails for youtube links to add
function processThreads(threads)
{
  var regexp =  new RegExp(".*v%3D([^%]+)%.*", "gi"); 
  var offlinePlaylistVideoIds = new Array();
  
  // build list of current playlist video ids
  var nextPageToken = '';
  while (nextPageToken != null) {
    var playlistResponse = YouTube.PlaylistItems.list('snippet', {
      playlistId: targetPlaylistId,
      maxResults: 25,
      pageToken: nextPageToken
    });
    for (var itemIndex = 0; itemIndex < playlistResponse.items.length; itemIndex++) {
      var playlistItem = playlistResponse.items[itemIndex];
      offlinePlaylistVideoIds.push(playlistItem.snippet.resourceId.videoId);
    }
    nextPageToken = playlistResponse.nextPageToken;
  }
  
  // loop through threads
  for (var i in threads) {
    try {
      messages = threads[i].getMessages()
      // loop through messages in thread
      for (var k in messages) {
        var msg = messages[k];
        var videoIds = regexp.exec(msg.getBody());
        var videoIdList = new Array();
        if (videoIds != null) {
          // get video ids in the message
          for(var match = 1; match < videoIds.length; match++)
          {
            videoIdList.push(videoIds[match]); 
          }
        }
        
        // for each video in the message
        for(var videoIndex = 0; videoIndex < videoIdList.length; videoIndex++){
          var videoId = videoIdList[videoIndex];
          var part= 'id';
          var details = {
            videoId: videoId,
            kind: 'youtube#video'
          };
          
          // get video object
          var videoResponse = YouTube.Videos.list('snippet, contentDetails',{id:videoId, maxResults:1});
          if(videoResponse.items.length > 0){
            var theVideo = videoResponse.items[0];
            // if ignore Upcoming Live is enabled, check video type and ignore upcoming announcements
            var liveContent = theVideo.snippet.liveBroadcastContent;
            if(ignoreUpcomingLive && liveContent == 'upcoming'){
              continue;
            }
            
            // check duration limits and ignore if under min or over max
            if(typeof minDuration !== 'undefined' && typeof maxDuration !== 'undefined'){
              var duration = theVideo.contentDetails.duration;
              var durationRegExp = new RegExp("P(?:([0-9]{0,3})D)?T(?:([0-9]{0,2})H)?(?:([0-9]{0,2})M)?(?:([0-9]{0,2})S)?", "ig");
              var durationResult = durationRegExp.exec(duration);
              var durationSeconds = 0;
              if(durationResult[4]){
                durationSeconds += parseInt(durationResult[4]);
              }
              if(durationResult[3]){
                durationSeconds += parseInt(durationResult[3]) * 60;
              }
              if(durationResult[2]){
                durationSeconds += parseInt(durationResult[2]) * 60 * 60;
              }
              if(durationResult[1]){
                durationSeconds += parseInt(durationResult[1]) * 24 * 60 * 60;
              }
              
              if(typeof minDuration !== 'undefined' && durationSeconds < minDuration){
                continue;
              }
              
              if(typeof maxDuration !== 'undefined' && durationSeconds > maxDuration){
                continue;
              }
            }
            
            // check if already in playlist and add if it isn't
            if(offlinePlaylistVideoIds.indexOf(videoId) >= 0 )
            {
              continue;
            }
            
            // if we made it this far, go ahead and add the video to the playlist
            var resource = {
              snippet: {
                playlistId: targetPlaylistId,
                resourceId: details
              }
            };
            YouTube.PlaylistItems.insert(resource, 'snippet');
            // mark the email read and archive it
            threads[i].markRead();
            threads[i].moveToArchive();
            if(typeof logsheetId !== 'undefined'){
              activeLogsheet.appendRow([Utilities.formatDate(new Date(), timezone, "yyyy-MM-dd HH:mm:ss '" + timezone +"'"),'Add Video',playlistName,theVideo.snippet.channelTitle,theVideo.snippet.title, Utilities.formatDate(new Date(theVideo.snippet.publishedAt), timezone,"yyyy-MM-dd HH:mm:ss '" + timezone +"'")]);
            }
            
          }// end individual video
        }// end video id iteration
      }// end message iteration
    }// end try
    
    catch(e) {
      Logger.log(e.message);
    } // end error 
  } // end thread loop
} // end process threads function


// removes videos that were published more than X days ago from the playlist
function removeOldVideos(days){
  var nextPageToken = '';
  var targetDate = new Date();
  
  targetDate.setDate(targetDate.getDate()-(days-1));
  targetDate.setHours(0);
  targetDate.setMinutes(0);
  while (nextPageToken != null) {
    var playlistResponse = YouTube.PlaylistItems.list('snippet', {
      playlistId: targetPlaylistId,
      maxResults: 25,
      pageToken: nextPageToken
    });
    // loop through videos in playlist
    for (var j = 0; j < playlistResponse.items.length; j++) {
      var playlistItem = playlistResponse.items[j];
      var itemDate = new Date(playlistItem.snippet.publishedAt);
      var videoResponse = YouTube.Videos.list('snippet',{id: playlistItem.snippet.resourceId.videoId, maxResults:1});
      if(videoResponse.items.length > 0){
        var theVideo = videoResponse.items[0];
        var theVideoDate = new Date(theVideo.snippet.publishedAt);
        if(theVideoDate.valueOf() < targetDate.valueOf()){
          YouTube.PlaylistItems.remove(playlistItem.id);
          if(typeof logsheetId !== 'undefined'){
            activeLogsheet.appendRow([Utilities.formatDate(new Date(), timezone, "yyyy-MM-dd HH:mm:ss '" + timezone +"'"),'Remove Video',playlistName,theVideo.snippet.channelTitle,theVideo.snippet.title, Utilities.formatDate(theVideoDate, timezone,"yyyy-MM-dd HH:mm:ss '" + timezone+"'")]);
          }//end logging
          
        }//end date range check
      }//end video id search loop 
    }//end playlist loop
    nextPageToken = playlistResponse.nextPageToken;
  }// end playlist tokenization loop
}// end RemoveOld Videos function